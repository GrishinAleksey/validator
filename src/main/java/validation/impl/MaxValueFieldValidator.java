package validation.impl;

import annotations.Max;
import entity.BrokenField;
import validation.Validator;

import java.lang.reflect.Field;

public class MaxValueFieldValidator implements Validator {

    @Override
    public BrokenField validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for(Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if(field.isAnnotationPresent(Max.class)){
                    Max an = field.getAnnotation(Max.class);
                    int max = an.value();
                    Integer value = (Integer) field.get(obj);
                    if(value > max){
                        return new BrokenField(field.getName(), value, "does not match MaxValue arguments", max);
                    }
            }
        }
        return null;
    }
}
