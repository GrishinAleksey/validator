package validation.impl;

import annotations.Min;
import entity.BrokenField;
import validation.Validator;

import java.lang.reflect.Field;

public class MinValueFieldValidator implements Validator {

    @Override
    public BrokenField validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for(Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if(field.isAnnotationPresent(Min.class)){
                    Min an = field.getAnnotation(Min.class);
                    int min = an.value();
                    Integer value = (Integer) field.get(obj);
                    if(value < min){
                        return new BrokenField(field.getName(), value, "does not match MaxValue arguments", min);
                    }
            }
        }
        return null;
    }
}
