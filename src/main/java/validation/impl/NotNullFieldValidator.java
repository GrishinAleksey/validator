package validation.impl;

import annotations.NotNull;
import entity.BrokenField;
import validation.Validator;

import java.lang.reflect.Field;

public class NotNullFieldValidator implements Validator {
    @Override
    public BrokenField validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for(Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if(field.isAnnotationPresent(NotNull.class)){
                    String value = (String) field.get(obj);
                    if(value == null){
                        return new BrokenField(field.getName(), value, "Value is null");
                    }
            }
        }
        return null;
    }
}
