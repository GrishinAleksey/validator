package validation.impl;

import annotations.PositiveValue;
import entity.BrokenField;
import validation.Validator;

import java.lang.reflect.Field;

public class PositiveValueValidator implements Validator {
    @Override
    public BrokenField validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for(Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if(field.isAnnotationPresent(PositiveValue.class)){
                    int value = (Integer) field.get(obj);
                    if(value < 0){
                        return new BrokenField(field.getName(), value, "Value is negative");
                    }
            }
        }
        return null;
    }
}
