package validation.impl;

import annotations.MaxLength;
import entity.BrokenField;
import validation.Validator;

import java.lang.reflect.Field;

public class MaxLengthFieldValidator implements Validator {

    @Override
    public BrokenField validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for(Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if(field.isAnnotationPresent(MaxLength.class)){
                    MaxLength an = field.getAnnotation(MaxLength.class);
                    int maxLength = an.max();
                    String value = field.get(obj).toString();
                    if(value.length() > maxLength){
                        return new BrokenField(field.getName(), value, "does not match MaxLength arguments", maxLength);
                    }
            }
        }
        return null;
    }
}
