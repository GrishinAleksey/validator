package validation.impl;

import entity.BrokenField;
import java.util.ArrayList;

public class ValidatorImpl{

    public void validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
        ArrayList<BrokenField> brokenFields = new ArrayList<>();
        brokenFields.add(new MaxValueFieldValidator().validate(obj));
        brokenFields.add(new MinValueFieldValidator().validate(obj));
        brokenFields.add(new MaxLengthFieldValidator().validate(obj));
        brokenFields.add(new MinLengthFieldValidator().validate(obj));
        brokenFields.add(new NotNullFieldValidator().validate(obj));
        brokenFields.add(new NotEmptyFieldValidator().validate(obj));
        brokenFields.add(new PositiveValueValidator().validate(obj));
        System.out.println(brokenFields);
    }
}
