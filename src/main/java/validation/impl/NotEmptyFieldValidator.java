package validation.impl;

import annotations.NotEmpty;
import entity.BrokenField;
import validation.Validator;

import java.lang.reflect.Field;

public class NotEmptyFieldValidator implements Validator {
    @Override
    public BrokenField validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for(Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if(field.isAnnotationPresent(NotEmpty.class)){
                    String value = (String) field.get(obj);
                    if(value.isEmpty()){
                        return new BrokenField(field.getName(), value, "value is Empty");
                    }
            }
        }
        return null;
    }
}
