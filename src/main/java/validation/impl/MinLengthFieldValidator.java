package validation.impl;

import annotations.MinLength;
import entity.BrokenField;
import validation.Validator;

import java.lang.reflect.Field;

public class MinLengthFieldValidator implements Validator {


    @Override
    public BrokenField validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for(Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if(field.isAnnotationPresent(MinLength.class)){
                    MinLength an = field.getAnnotation(MinLength.class);
                    int min = an.min();
                    String value = field.get(obj).toString();
                    if(value.length() < min){
                        return new BrokenField(field.getName(), value, "does not match Minlength arguments", min);
                    }
            }
        }
        return null;
    }
}
