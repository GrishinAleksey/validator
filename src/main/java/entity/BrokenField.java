package entity;

import java.util.Arrays;

public class BrokenField {
    private final String fieldName;
    private final Object fieldValue;
    private final String violatedRule;
    private final Object[] args;

    public BrokenField(String fieldName, Object fieldValue, String violatedRule, Object... args) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.violatedRule = violatedRule;
        this.args = args;
    }

    @Override
    public String toString() {
        return "{\"_class\":\"BrokenField\", " +
                "\"fieldName\":" + (fieldName == null ? "null" : "\"" + fieldName + "\"") + ", " +
                "\"fieldValue\":" + (fieldValue == null ? "null" : "\"" + fieldValue + "\"") + ", " +
                "\"violatedRule\":" + (violatedRule == null ? "null" : "\"" + violatedRule + "\"") + ", " +
                "\"args\":" + Arrays.toString(args) +
                "}";
    }

}
